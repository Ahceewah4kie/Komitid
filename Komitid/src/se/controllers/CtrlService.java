package se.controllers;

import java.util.ArrayList;

import se.apihandlers.FacebookHandler;
import se.entities.Checkpoint;
import se.komitid.R;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import se.storage.Logger;
import se.storage.SharedPrefsHandler;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * A controller for the LocalService.
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class CtrlService {
	private Context context = ContextPasser.getLocalContext();
	private final String tag = "Komitid | CtrlCheckpoint";
	private DBAdapter db;
	private boolean lastCheckpointReached = false;

	public CtrlService(DBAdapter db) {
		this.db = db;
	}

	/**
	 * Checks if a checkpoint has been reached.
	 * 
	 * @param usrLat
	 *            - users current latitude
	 * @param usrLng
	 *            - users current longitude
	 * @param checkpointLat
	 *            - latitude of current checkpoint
	 * @param checkpointLng
	 *            - longitude of current checkpoint
	 * @return true if users location is less than 25 meters from the checkpoint
	 */
	public boolean hasClosestCheckpointBeenReached(double usrLat, double usrLng, double checkpointLat, double checkpointLng) {
		boolean isReached = false;
		double distance = getDistance(usrLat, usrLng, checkpointLat, checkpointLng, 'M');
		Log.i(tag, "distance to cp=" + distance);
		if (distance < 25)
			isReached = true;
		return isReached;
	}

	/**
	 * Returns the distance between two coordinates.
	 * 
	 * @param lat1
	 *            - latitude of point A
	 * @param lon1
	 *            - longitude of point A
	 * @param lat2
	 *            - latitude of point B
	 * @param lon2
	 *            - latitude of point B
	 * @param unit
	 *            - K equals kilometers, M equals miles, N equals nautical
	 *            miles, M equals meters
	 * @return
	 */
	private double getDistance(double lat1, double lon1, double lat2, double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
				* Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		} else if (unit == 'M') {
			dist = dist * 1609.344;
		}
		return (dist);
	}

	// converts decimal degrees to radians
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	// converts radians to decimal degrees
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	/**
	 * Gets the current checkpoints' latitude, longitude and time. TODO: Do this
	 * by a query instead of fucking around with Java.
	 * 
	 * @return the current checkpoints' latitude, longitude and time.
	 */
	public Checkpoint getCurrentCheckpoint() {
		ArrayList<Checkpoint> cpArray = getAllCheckpoints();
		Checkpoint currentCp = new Checkpoint();
		int currentCpNbr = SharedPrefsHandler.getCurrentCheckpoint();

		if (cpArray.size() > 0 && currentCpNbr < cpArray.size()) {
			currentCp = cpArray.get(currentCpNbr);
			Log.i(tag, "current cp= " + currentCpNbr);
			Log.i(tag, "cp lat= " + currentCp.getLat());
			Log.i(tag, "cp lng= " + currentCp.getLng());
			Log.i(tag, "cp name= " + currentCp.getName());
			Log.i(tag, "cp time= " + currentCp.getTime());

		} else {
			setLastCheckpointReached(true);
		}
		Logger.i(tag, "CURRENT CHECKPOINT=" + currentCp);
		return currentCp;
	}

	/**
	 * Returns true if last checkpoint has been reached.
	 * 
	 * @return true if last checkpoint has been reached.
	 */
	public boolean isLastCheckpointReached() {
		return lastCheckpointReached;
	}

	/**
	 * Setter for lastCheckpointReached
	 * 
	 * @param lastCheckpointReached
	 */
	public void setLastCheckpointReached(boolean lastCheckpointReached) {
		this.lastCheckpointReached = lastCheckpointReached;
	}

	/**
	 * Returns an arraylist of all checkpoints.
	 * 
	 * @return Returns an arraylist of all checkpoints.
	 */
	public ArrayList<Checkpoint> getAllCheckpoints() {
		ArrayList<Checkpoint> cpArray = new ArrayList<Checkpoint>();

		db.open();
		Cursor dep = db.getAllDepartures();
		Cursor arr = db.getAllArrivls();
		while (dep.moveToNext()) {
			Checkpoint checkpoint = new Checkpoint();
			checkpoint.setSegment(dep.getInt(0));
			checkpoint.setLat(dep.getDouble(1));
			checkpoint.setLng(dep.getDouble(2));
			checkpoint.setName(dep.getString(3));
			checkpoint.setTime(dep.getLong(4));
			cpArray.add(checkpoint);
		}

		if (arr.moveToLast()) {
			Checkpoint checkpoint = new Checkpoint();
			checkpoint.setSegment(arr.getInt(0));
			checkpoint.setLat(arr.getDouble(1));
			checkpoint.setLng(arr.getDouble(2));
			checkpoint.setName(arr.getString(3));
			checkpoint.setTime(arr.getLong(4));
			Logger.i(tag, "LAST CHECKPOINT=" + checkpoint);
			cpArray.add(checkpoint);
		} else {
			Logger.i(tag, "arr.moveToLast() körs inte, detta är ett problem");
		}

		for (Checkpoint check : cpArray) {
			Logger.i(tag, check.toString());
		}
		db.close();

		return cpArray;
	}

	public boolean isUserOnSchedule(long time) {
		long currentTime = System.currentTimeMillis();
		if (currentTime > time) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Called if user is late. Posts a pre-written(?) message to the users wall.
	 */
	public void userIsLate() {
		FacebookHandler fb = new FacebookHandler();
		fb.publishMessage(context.getString(R.string.facebook_islatemessage));
	}

	/**
	 * Called if user has reached a checkpoint, but not the last checkpoint. Add
	 * +1 to the users has-reached-checkpoint-in-time-count and adds +1 to which
	 * checkpoint user is currently heading to. Also adds +1 to segment if the
	 * checkpoint-number is even, since there's two checkpoints in each segment.
	 */
	public void userHasReachedCheckpoint() {
		SharedPrefsHandler.addUserHasBeenOnTime();
		SharedPrefsHandler.addCurrentCheckpoint();
		if (SharedPrefsHandler.getCurrentCheckpoint() % 2 == 0)
			SharedPrefsHandler.addCurrentSegment();
	}

	/**
	 * Called when user has reached it's final checkpoint. Sets the trip as
	 * inactive.
	 */
	public void userHasReachedFinalCheckpoint() {
		SharedPrefsHandler.setActiveTrip(false);
	}

}
