package se.controllers;

import java.util.ArrayList;

import se.komitid.R;
import se.others.ContextPasser;
import se.storage.SharedPrefsHandler;
import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Controller class for FragMap
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class CtrlrMap {
	private Context context = ContextPasser.getLocalContext();
	
	/**
	 * Adds a marker to the map.
	 * @param markers - arraylist of markers
	 * @param location - location of markers
	 * @return an arraylist of the markeroptions
	 */
	public ArrayList<MarkerOptions> addMarker(ArrayList<MarkerOptions> markers, LatLng location) {
		if (markers.size() >= 2) {
			markers.clear();
		}
		if (markers.size() < 1) {
			markers.add(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_mal_marker)).anchor(0.35f, 0.9f).position(new LatLng(location.latitude, location.longitude)));
			Toast.makeText(context, R.string.add_start_position, Toast.LENGTH_SHORT).show();
		} else if (markers.size() < 2) {
			markers.add(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_start_marker)).anchor(0.35f, 0.9f).position(new LatLng(location.latitude, location.longitude)));
		}
		return markers;
    }

	/**
	 * Saves locations to shared prefs.
	 * @param markers - arraylist of markers
	 */
	public void saveLocations(ArrayList<MarkerOptions> markers) {
		SharedPrefsHandler.setStartLatitude(markers.get(0).getPosition().latitude);
		SharedPrefsHandler.setStartLongitude(markers.get(0).getPosition().longitude);
		SharedPrefsHandler.setDestLatitude(markers.get(1).getPosition().latitude);
		SharedPrefsHandler.setDestLongitude(markers.get(1).getPosition().longitude);
		Toast.makeText(context, R.string.saved_destinations, Toast.LENGTH_SHORT).show();
    }
}
