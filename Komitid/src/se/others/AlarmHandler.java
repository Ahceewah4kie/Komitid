package se.others;

import se.komitid.MainActivity;
import se.service.LocalService;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * A handler class for the AlarmManager functionality. This class sets and
 * cancels alarms, as well as receives the later Intents sent by the
 * AlarmManager.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class AlarmHandler extends BroadcastReceiver {
	private final String tag = "Komitid | AlarmHandler";

	@Override
	public void onReceive(Context context, Intent intent) {
		// Starts LocalService
		Intent serviceIntent = new Intent(context, LocalService.class);
		context.startService(serviceIntent);

		// Starts the application/Brings it to the front
		Intent activityIntent = new Intent(context, MainActivity.class);
		activityIntent.putExtra("ALARM_RINGING", "ALARM_RINGING");
		activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(activityIntent);
		Log.i(tag, "DATA RECEIVED");
	}

	/**
	 * Sets a new alarm
	 * 
	 * @param context
	 *            Reference to the application context
	 * @param alarmTime
	 *            The time when the alarm should go off (milliseconds)
	 */
	public void setAlarm(Context context, long alarmTime) {
		AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent("komitid.SET_ALARM");
		PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
		am.set(AlarmManager.RTC_WAKEUP, alarmTime, pi);
	}

	/**
	 * Cancels the alarm, if set
	 * 
	 * @param context
	 *            Reference to the application context.
	 */
	public void cancelAlarm(Context context) {
		Intent intent = new Intent("komitid.SET_ALARM");
		PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(sender);
	}

}
