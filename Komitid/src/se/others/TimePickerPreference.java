package se.others;

import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

/**
 * Creates a custom preference where the user can define an amount of time using
 * hours and minutes.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class TimePickerPreference extends DialogPreference {

	private NumberPicker pickerHour;
	private NumberPicker pickerMinute;

	public TimePickerPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSummary(R.string.hours_and_minutes);
		setDialogLayoutResource(R.layout.layout_time_preference);
		setPositiveButtonText(R.string.ok);
		setNegativeButtonText(R.string.cancel);
		setDialogIcon(null);
	}

	/**
	 * Initializes the components before displaying the dialog.
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		initComponents(view);
	}

	/**
	 * Converts the hours and minutes to milliseconds and stores the value in
	 * SharedPreferences. This method is called before the dialog is closed.
	 */
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		if (positiveResult) {
			SharedPrefsHandler.setPreparationTime(pickersToMillis());
		}
	}

	/**
	 * Initializes the required components. Gets called from onBindDialogView.
	 * 
	 * @param view
	 */
	private void initComponents(View view) {
		pickerHour = (NumberPicker) view.findViewById(R.id.np_hour_picker);
		pickerMinute = (NumberPicker) view.findViewById(R.id.np_minute_picker);

		pickerHour.setMinValue(0);
		pickerHour.setMaxValue(23);
		pickerMinute.setMinValue(0);
		pickerMinute.setMaxValue(59);

		setPickersFromPrefs();
	}

	/**
	 * Converts the currently selected values in the pickers (hours and minutes)
	 * to milliseconds.
	 * 
	 * @return The value in the pickers in milliseconds.
	 */
	private long pickersToMillis() {
		int hour = pickerHour.getValue();
		int minute = pickerMinute.getValue();
		Log.i("Tid", "Timme: " + hour + ", Minut: " + minute);
		long millis = (hour * 3600000) + (minute * 60000);

		return millis;
	}

	/**
	 * Sets the values in the pickers to correspond to the value currently
	 * stored in SharedPreferences (0 otherwise). The method converts from
	 * milliseconds to hours and minutes.
	 */
	private void setPickersFromPrefs() {
		int hour = 0;
		int minute = 0;
		long millis = SharedPrefsHandler.getPreparationTime();

		if (millis != 0) {
			long minMillis = millis % 3600000; // Minutes in millis
			long hourMillis = millis - minMillis; // Hours in millis

			hour = (int) hourMillis / 3600000; // Actual hours
			minute = (int) minMillis / 60000; // Actual minutes
		}

		pickerHour.setValue(hour);
		pickerMinute.setValue(minute);
	}

}
