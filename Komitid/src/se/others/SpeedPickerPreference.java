package se.others;

import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.NumberPicker;

/**
 * Creates a custom preference where the user can define the desired walking
 * speed in km/h.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class SpeedPickerPreference extends DialogPreference {

	private NumberPicker pickerSpeed;

	public SpeedPickerPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		setSummary(R.string.km_per_hour);
		setDialogLayoutResource(R.layout.layout_speed_preference);
		setPositiveButtonText(R.string.ok);
		setNegativeButtonText(R.string.cancel);
		setDialogIcon(null);
	}

	/**
	 * Initializes the components before displaying the dialog.
	 */
	@Override
	protected void onBindDialogView(View view) {
		super.onBindDialogView(view);
		initComponents(view);
	}

	/**
	 * Stores the chosen value in SharedPreferences.
	 */
	protected void onDialogClosed(boolean positiveResult) {
		if (positiveResult) {
			SharedPrefsHandler.setWalkingSpeed(pickerSpeed.getValue());
		}
	}

	/**
	 * Initializes the components.
	 * 
	 * @param view
	 */
	private void initComponents(View view) {
		this.pickerSpeed = (NumberPicker) view
				.findViewById(R.id.np_speed_picker);

		pickerSpeed.setMinValue(0);
		pickerSpeed.setMaxValue(20);
		setPickerFromPrefs();
	}

	/**
	 * Sets the default value for the picker, based on the value stored in
	 * SharedPreferences.
	 */
	private void setPickerFromPrefs() {
		pickerSpeed.setValue((int) SharedPrefsHandler.getWalkingSpeed());
	}
}
