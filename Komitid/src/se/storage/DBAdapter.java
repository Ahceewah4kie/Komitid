package se.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class acts handles the communications with the database itself, acting
 * as an API of sorts.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class DBAdapter {

	private SQLiteDatabase database;
	private DatabaseHelper helper;
	private final String tag = "Komitid | DBAdapter";

	public DBAdapter(Context context) {
		helper = new DatabaseHelper(context);
	}
	
	/**
	 * Adds a departure to db
	 * @param segment - segment of depature
	 * @param lat - latitude of depature
	 * @param lng - longitude of depature
	 * @param name - name of depature
	 * @param datetime - time of depature
	 * @param transportMethod - transport method of depature
	 * @param distance - distance to arrival
	 * @param number - number of the transport method
	 * @param id - unique id of the transport method
	 * @param depCarrierUrl - url to the carrier company
	 * @param depCarrierName - name of the carrier company
	 */
	public void addDeparture(int segment, double lat, double lng, String name, long datetime, String transportMethod, int distance, String number,
	        String id, String depCarrierUrl, String depCarrierName) {
		try {
			ContentValues vals = new ContentValues();
			vals.put("segment", segment);
			vals.put("lat", lat);
			vals.put("lng", lng);
			vals.put("name", name);
			vals.put("datetime", datetime);
			vals.put("transportmethod", transportMethod);
			vals.put("distance", distance);
			vals.put("number", number);
			vals.put("transport_id", id);
			vals.put("carrier_url", depCarrierUrl);
			vals.put("carrier_name", depCarrierName);

			Log.i(tag, "############\nSaved to db (departure):\nSegment=" + segment + "\nLat=" + lat + "\nLng=" + lng + "\nName=" + name
			        + "\nDatetime=" + datetime + "\ntransportmethod=" + transportMethod + "\ndistance=" + distance + "\n############");
			database.insert("departure", null, vals);
		} catch (SQLException e) {
			Log.i(tag, "############\nFailed to save to db (departure):\nSegment=" + segment + "\nLat=" + lat + "\nLng=" + lng + "\nName=" + name
			        + "\nDatetime=" + datetime + "\ntransportmethod=" + transportMethod + "\ndistance=" + distance + "\n############");
		}
	}

	/**
	 * Adds an arrival to db
	 * @param segment - segment of arrival
	 * @param lat - latitude of arrival
	 * @param lng - longitude of arrival
	 * @param name - name of place of arrival
	 * @param datetime - time of arrival
	 * @param id - unique id of arrival
	 */
	public void addArrival(int segment, double lat, double lng, String name, long datetime, int id) {
		try {
			ContentValues vals = new ContentValues();
			vals.put("segment", segment);
			vals.put("lat", lat);
			vals.put("lng", lng);
			vals.put("name", name);
			vals.put("datetime", datetime);
			vals.put("arrID", id);
			Log.i(tag, "############\nSaved to db (arrival):\nSegment=" + segment + "\nLat=" + lat + "\nLng=" + lng + "\nName=" + name
			        + "\nDatetime=" + datetime + "\narrID=" + id + "\n############");
			database.insert("arrival", null, vals);
		} catch (SQLException e) {
			Log.i(tag, "############\nFailed to save to db (arrival): \nSegment=" + segment + "\nLat=" + lat + "\nLng=" + lng + "\nName=" + name
			        + "\nDatetime=" + datetime + "\narrID=" + id + "\n############");
		}

	}

	/**
	 * Returns the departure of a segment
	 * @param segment - the segment of the departure you want to get.
	 * @return a cursor, pointing at the departure of the segment
	 */
	public Cursor getDeparture(int segment) {
		Cursor c = database.rawQuery("SELECT * FROM departure WHERE segment ='" + segment + "' ORDER BY segment LIMIT 1", null);
		Log.i(tag, "Returning next departure");
		return c;
	}

	/**
	 * Returns the arrival of a segment
	 * @param segment - the segment of the arrival you want to get.
	 * @return a cursor, pointing at the arrival of the segment
	 */
	public Cursor getArrival(int segment) {
		Cursor c = database.rawQuery("SELECT * FROM arrival WHERE segment = '" + segment + "' ORDER BY segment LIMIT 1", null);
		Log.i(tag, "Returning next arrival");
		return c;
	}

	/**
	 * Returns all the departures
	 * @return a cursor, pointing at the first departure
	 */
	public Cursor getAllDepartures() {
		Cursor c = database.rawQuery("SELECT * FROM departure ORDER BY segment", null);
		Log.i(tag, "Returning all departures");
		return c;
	}
	
	/**
	 * Returns all the arrivals
	 * @return a cursor, pointing at the first arrival
	 */
	public Cursor getAllArrivls() {
		Cursor c = database.rawQuery("SELECT * FROM arrival ORDER BY segment", null);
		Log.i(tag, "Returning all arrivals");
		return c;
	}

	/**
	 * Returns the time of the first departure
	 * @return a cursor, pointing at the time of the first departure
	 */
	public Cursor getFirstDepartureTime() {
		Cursor c = database.rawQuery("SELECT datetime FROM departure ORDER BY segment LIMIT 1", null);
		Log.i(tag, "Returning all arrivals");
		return c;
	}

	/**
	 * Drops all departures
	 */
	public void dropDepartures() {
		database.delete("departure", null, null);
		Log.i(tag, "DROPPING ALL DEPARTURES");
	}

	/**
	 * Drops all arrivals
	 */
	public void dropArrivals() {
		database.delete("arrival", null, null);
		Log.i(tag, "DROPPING ALL ARRIVALS");
	}

	/**
	 * Makes the database writable
	 * @return a database helper
	 */
	public DBAdapter open() {
		database = helper.getWritableDatabase();
		Log.i(tag, "Opening db...");
		return this;
	}

	/**
	 * Write protects the database.
	 */
	public void close() {
		database.close();
		Log.i(tag, "db closed!");
	}

	/**
	 * Inner class that handles the creation and maintenance of the database.
	 * 
	 * @author Fredrik Andersson 
	 * @author Simon Cedergren <simon@tuxflux.se>
	 */
	private class DatabaseHelper extends SQLiteOpenHelper {

		private static final String NAME = "KomitidDB";
		private static final String CREATE_DEPARTURE = "CREATE TABLE departure (" + "segment INTEGER, " + "lat DOUBLE, " + "lng DOUBLE, "
		        + "name TEXT, " + "datetime LONG, " + "distance INTEGER, " + "transportmethod TEXT, " + "number TEXT, " + "transport_id TEXT, "
		        + "carrier_url TEXT, " + "carrier_name TEXT, " + "id INTEGER PRIMARY KEY AUTOINCREMENT);";
		private static final String CREATE_ARRIVAL = "CREATE TABLE arrival (" + "segment INTEGER, " + "lat DOUBLE, " + "lng DOUBLE, " + "name TEXT, "
		        + "datetime LONG, " + "arrID INTEGER, " + "id INTEGER PRIMARY KEY AUTOINCREMENT);";
		private static final int VERSION = 2;

		public DatabaseHelper(Context context) {
			super(context, NAME, null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_DEPARTURE);
			db.execSQL(CREATE_ARRIVAL);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE arrival;");
			db.execSQL("DROP TABLE departure;");
			db.execSQL(CREATE_DEPARTURE);
			db.execSQL(CREATE_ARRIVAL);
		}
	}
}