package se.fragments;

import se.controllers.CtrlrSetGoal;
import se.komitid.MainActivity;
import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * The fragment containing the GUI for the "Set Goal"-screen, where the user
 * chooses to set destination, set arrival time, and then start the trip.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class FragSetGoal extends Fragment {

	private View view;
	private ImageView btnDestination;
	private ImageView btnArrivalTime;
	private Button btnOk;

	private MainActivity main;
	private CtrlrSetGoal controller;

	/**
	 * Creates a new instance of this fragment, acting as a constructor.
	 * @param main A reference to MainActivity
	 * @return A new instance of FragSetGoal
	 */
	public static FragSetGoal newInstance(MainActivity main) {
		FragSetGoal frag = new FragSetGoal();
		frag.setMainActivity(main);
		return frag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.view = inflater.inflate(R.layout.layout_set_goal, null);
		this.getActivity().getActionBar().setTitle(R.string.set_goal);

		initComponents();
		registerListeners();

		return view;
	}

	/**
	 * Stores the reference to MainActivity.
	 * @param main A reference to MainActivity.
	 */
	public void setMainActivity(MainActivity main) {
		this.main = main;
	}

	/**
	 * Initializes the components used in this fragment.
	 */
	private void initComponents() {
		controller = new CtrlrSetGoal();

		btnDestination = (ImageView) view.findViewById(R.id.ivDestination);
		btnArrivalTime = (ImageView) view.findViewById(R.id.ivArrivalTime);
		btnOk = (Button) view.findViewById(R.id.btnOk);

	}

	/**
	 * Registers the listeners for the buttons.
	 */
	private void registerListeners() {
		btnDestination.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				main.switchFragment(new FragMap());
			}
		});

		btnArrivalTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				main.showDialogFragment(new DiagSetArrival());
			}
		});

		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				AlertDialog.Builder diagConfirm = new AlertDialog.Builder(getActivity());
				diagConfirm.setMessage("Påbörja nedräkning?");
				diagConfirm.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (SharedPrefsHandler.tripIsActive() || SharedPrefsHandler.getAlarmTime() != 0) {
							Toast.makeText(getActivity(), R.string.already_started_trip, Toast.LENGTH_SHORT).show();
						} else if (SharedPrefsHandler.getDestLatitude() == 0) {
							Toast.makeText(getActivity(), R.string.pick_destination_first, Toast.LENGTH_SHORT).show();
						} else {
							SharedPrefsHandler.resetCurrentCheckpoint();
							SharedPrefsHandler.resetCurrentSegment();
							controller.sendQueryFromPrefs();
							dialog.dismiss();
						}
					}

				});

				diagConfirm.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				});

				diagConfirm.show();
			}
		});
	}

}
