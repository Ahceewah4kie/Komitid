package se.fragments;

import java.util.ArrayList;

import se.controllers.CtrlrMap;
import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.app.Dialog;
import android.app.Fragment;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * The fragment containing the map where the user can select the destination and
 * starting point of the trip.
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class FragMap extends Fragment implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	private final String tag = "Maps | FragMap";
	private View view;
	private GoogleMap map;
	private LocationRequest locationReq;
	private LocationClient locationClient;
	private double lat = -1, lng = -1;
	private boolean hasBeenCentered = false;
	private Button btnSave, btnCancel;
	private ArrayList<MarkerOptions> markers;
	private double depLat, depLng, arrLat, arrLng;
	private BitmapDescriptor icon_departure;
	private BitmapDescriptor icon_arrival;
	private CtrlrMap ctrlrMap;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (view != null) { // Courtesy of Vidar Wahlberg @ bit.ly/1iGphpm
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}
		try {
			this.view = inflater.inflate(R.layout.layout_map_goalpicker, container, false);
		} catch (InflateException e) {
		}
		ctrlrMap = new CtrlrMap();
		this.getActivity().getActionBar().setTitle(R.string.set_destination);
		Toast.makeText(getActivity(), R.string.click_to_add_destination, Toast.LENGTH_SHORT).show();
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.main, menu);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.gp_map);
		if (f != null)
			getFragmentManager().beginTransaction().remove(f).commit();
	}

	@Override
	public void onResume() {
		initComponents();
		registerButtonListerners();
		registerMapListener();
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		locationClient.disconnect();
		super.onStop();
	}

	/**
	 * Initializes the required components.
	 */
	private void initComponents() {
		btnCancel = (Button) view.findViewById(R.id.btn_map_cancel);
		btnSave = (Button) view.findViewById(R.id.btn_map_save);
		markers = new ArrayList<MarkerOptions>();
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.gp_map)).getMap();
		locationClient = new LocationClient(getActivity(), this, this);
		map.setMapType(3); // Road view.
		if (isServicesConnected()) {
			locationReq = LocationRequest.create();
			locationReq.setInterval(60000);
			locationReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		}
		locationClient.connect();
	}

	@Override
	public void onLocationChanged(Location location) {
		lat = location.getLatitude();
		lng = location.getLongitude();

		if (!hasBeenCentered) {
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10);
			map.animateCamera(cameraUpdate);
			hasBeenCentered = true;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(getActivity(), getString(R.string.error_connecting_gps), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		try {
			Thread.sleep(100); // TODO: Fulhack. This lets the device to finish
								// the connection.
			locationClient.requestLocationUpdates(locationReq, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(getActivity(), getString(R.string.disconnected_from_gps), Toast.LENGTH_SHORT).show();
	}

	/**
	 * Checks if GooglePlay services are available.
	 * 
	 * @return 'true' if available, otherwise 'false'.
	 */
	private boolean isServicesConnected() {
		boolean isConnected = false;
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		if (ConnectionResult.SUCCESS == resultCode) {
			isConnected = true;
		} else {
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 9000);
			if (errorDialog != null) {
				errorDialog.show();
			}
			isConnected = false;
		}
		return isConnected;
	}

	/**
	 * Registers the onMapClick-listener for the map.
	 */
	private void registerMapListener() {
		map.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng loc) {
				ctrlrMap.addMarker(markers, loc);
				map.clear();
				for (MarkerOptions marker : markers) {
					map.addMarker(marker);
				}
			}
		});
	}

	/**
	 * Registers the button listeners for the fragment.
	 */
	private void registerButtonListerners() {
		btnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ctrlrMap.saveLocations(markers);
				for (MarkerOptions marker : markers) {
					map.addMarker(marker);
				}
				getFragmentManager().popBackStack();
			}
		});

		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStack();
			}
		});
	}

}
