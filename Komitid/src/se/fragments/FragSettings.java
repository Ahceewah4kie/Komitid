package se.fragments;

import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

/**
 * This fragment contains the Settings screen of the application.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class FragSettings extends PreferenceFragment {

	private CheckBoxPreference checkBus;
	private CheckBoxPreference checkTrain;
	private CheckBoxPreference checkBoat;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.layout_settings);
	}

	@Override
	public void onResume() {
		this.getActivity().getActionBar().setTitle(R.string.settings);

		checkBus = (CheckBoxPreference) findPreference("check_bus");
		checkTrain = (CheckBoxPreference) findPreference("check_train");
		checkBoat = (CheckBoxPreference) findPreference("check_boat");

		checkBus.setDefaultValue(true);
		checkTrain.setDefaultValue(true);
		checkBoat.setDefaultValue(true);
		setCheckBoxFromPrefs();
		registerCheckBoxListeners();
		super.onResume();
	}

	/**
	 * Registers onClick-listeners for the CheckBoxPreferences.
	 */
	private void registerCheckBoxListeners() {
		checkBus.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				SharedPrefsHandler.setEnableBus(preference.getSharedPreferences().getBoolean("check_bus", true));
				return false;
			}
		});

		checkTrain.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				SharedPrefsHandler.setEnableTrain(preference.getSharedPreferences().getBoolean("check_train", true));
				return false;
			}
		});

		checkBoat.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				SharedPrefsHandler.setEnableBoat(preference.getSharedPreferences().getBoolean("check_boat", true));
				return false;
			}
		});
	}

	/**
	 * Restores the check box values from what's stored in SharedPreferences.
	 */
	private void setCheckBoxFromPrefs() {
		checkBus.setChecked(SharedPrefsHandler.getBusEnabled());
		checkTrain.setChecked(SharedPrefsHandler.getTrainEnabled());
		checkBoat.setChecked(SharedPrefsHandler.getBoatEnabled());
	}

}
