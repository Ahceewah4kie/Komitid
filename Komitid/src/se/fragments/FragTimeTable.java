package se.fragments;

import se.controllers.CtrlrTimeTable;
import se.komitid.R;
import se.others.ContextPasser;
import se.others.SegmentAdapter;
import se.storage.SharedPrefsHandler;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;


/**
 * A fragment that presents the current timetable
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class FragTimeTable extends Fragment {

	private View view;
	private ListView lv;
	private SegmentAdapter adapter;
	private CtrlrTimeTable controller;
	private Context context = ContextPasser.getLocalContext();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		// If there is a timetable in db, present it - else 
		// tell the user that there is none.
		if (SharedPrefsHandler.getDestLatitude() != 0) {
			view = inflater.inflate(R.layout.layout_time_table, null);
		} else {
			view = inflater.inflate(R.layout.layout_time_table_empty, null);
		}
		//
		setHasOptionsMenu(true);
		getActivity().getActionBar().setTitle(R.string.time_table);
		controller = new CtrlrTimeTable();
		return view;
	}

	@Override
	public void onResume() {
		
		// If there is a timetable in db, present it - else 
		// tell the user that there is none.
		if (SharedPrefsHandler.getDestLatitude() != 0) {
			lv = (ListView) view.findViewById(R.id.lv_departures);
			adapter = new SegmentAdapter(getActivity(), R.layout.row_checkpoint, controller.getAllSegments());
			lv.setAdapter(adapter);
		}else {
			ImageView iv = (ImageView)view.findViewById(R.id.iv_no_time_table);
			iv.setImageDrawable(getResources().getDrawable(R.drawable.ic_unknown));
		}
		//
		super.onResume();
	}
}
