package se.fragments;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import se.komitid.R;
import se.storage.SharedPrefsHandler;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TimePicker;
import android.widget.Toast;

/**
 * A dialog that allows the user to choose a time of day (hour and minute),
 * which gets stored in Shared Preferences on confirmation.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class DiagSetArrival extends DialogFragment {

	private TimePicker pickerArrival;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		builder.setTitle(getActivity().getString(R.string.set_arrival_time));
		builder.setView(inflater.inflate(R.layout.layout_set_arrival, null)).setPositiveButton(getString(R.string.ok), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				SharedPrefsHandler.setArrivalTime(pickerToDateTime());
				storePickerValues();
				Toast.makeText(getActivity(), R.string.arrival_time_saved, Toast.LENGTH_SHORT).show();
			}
		}).setNegativeButton(android.R.string.cancel, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dismiss();
			}
		});

		return builder.create();
	}

	@Override
	public void onResume() {
		super.onResume();
		initComponents();
		setPickerFromPrefs();
	}

	/**
	 * Initializes the components used in the class.
	 */
	private void initComponents() {
		pickerArrival = (TimePicker) getDialog().findViewById(R.id.pickerArrival);
		pickerArrival.setIs24HourView(true);
	}

	/**
	 * Retrieves the values (hours and minutes) from the picker, and converts
	 * them to milliseconds. The value is then added to the millisecond of
	 * 00:00:00 of the current day, if the selected hour/minute combination has
	 * not already passed. If it has passed the millisecond-value is added to
	 * 00:00:00 of the next day, instead.
	 * 
	 * @return The selected hour/minute combination in milliseconds, added to
	 *         either the current day, or the next day (if the selected
	 *         combination has already passed on the current day).
	 */
	private long pickerToDateTime() {
		int hour = pickerArrival.getCurrentHour();
		int min = pickerArrival.getCurrentMinute();
		long millis = 0;

		LocalTime time = new LocalTime(hour, min);
		if (time.toDateTimeToday().getMillis() <= System.currentTimeMillis()) {
			DateTime tomorrow = time.toDateTimeToday().plusDays(1);
			tomorrow = tomorrow.minusHours(tomorrow.getHourOfDay());
			tomorrow = tomorrow.minusMinutes(tomorrow.getMinuteOfDay());
			tomorrow = tomorrow.plusHours(hour);
			tomorrow = tomorrow.plusMinutes(min);
			millis = tomorrow.getMillis();
		} else {
			DateTime today = time.toDateTimeToday();
			today = today.minusHours(today.getHourOfDay());
			today = today.minusMinutes(today.getMinuteOfDay());
			today = today.plusHours(hour);
			today = today.plusMinutes(min);
			millis = today.getMillis();
		}
		return millis;
	}

	/**
	 * Sets the values in the picker to correspond to the value currently stored
	 * in SharedPreferences (0 otherwise). The method converts from milliseconds
	 * to hours and minutes.
	 */
	private void setPickerFromPrefs() {
		int hour = SharedPrefsHandler.getArrivalPickerHour();
		int minute = SharedPrefsHandler.getArrivalPickerMinute();

		pickerArrival.setCurrentHour(hour);
		pickerArrival.setCurrentMinute(minute);
	}

	/**
	 * Stores the values of the picker in Shared Preferences.
	 */
	private void storePickerValues() {
		int hour = pickerArrival.getCurrentHour();
		int minute = pickerArrival.getCurrentHour();

		SharedPrefsHandler.setArrivalPickerHour(hour);
		SharedPrefsHandler.setArrivalPickerMinute(minute);
	}

}
