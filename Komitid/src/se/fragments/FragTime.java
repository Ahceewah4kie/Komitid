package se.fragments;

import se.controllers.CtrlrTime;
import se.komitid.R;
import se.others.AlarmHandler;
import se.others.ContextPasser;
import se.storage.SharedPrefsHandler;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A fragment responsible for displaying various time info. What's displayed
 * varies depending on the state of the app, and includes the time the alarm
 * will go off, remaining preparation time and time until the next departure.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class FragTime extends Fragment {

	private final String tag = "Komitid | FragTime";

	private View view;
	private Button btnCancel;
	private AlarmHandler alarm;
	private TextView tvAlarmTime;
	private TextView tvLabel;
	private boolean isInflated = false;
	private CtrlrTime controller;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		alarm = new AlarmHandler();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		this.view = inflater.inflate(R.layout.layout_alarm, null);
		this.getActivity().getActionBar().setTitle(R.string.time_info);
		initComponents();
		registerListener();
		handleStates();
		return view;
	}

	/**
	 * Allows other classes to check if this fragment is inflated/visible.
	 * 
	 * @return 'true' if the fragment is inflated, otherwise 'false'.
	 */
	public boolean isInflated() {
		return isInflated;
	}

	@Override
	public void onResume() {
		super.onResume();
		alarm = new AlarmHandler();
		isInflated = true;
	}

	@Override
	public void onPause() {
		super.onPause();
		isInflated = true;
	}

	/**
	 * Initializes the components used by the fragment.
	 */
	private void initComponents() {
		this.btnCancel = (Button) view.findViewById(R.id.btnCancel);
		this.tvAlarmTime = (TextView) view.findViewById(R.id.tvAlarmTime);
		this.tvLabel = (TextView) view.findViewById(R.id.tvLabel);

		// Hides the button by default.
		btnCancel.setVisibility(View.GONE);
	}

	private void handleStates() {
		if (SharedPrefsHandler.getAppState() == SharedPrefsHandler.STATE_IDLE) {
			tvLabel.setText("");
			tvAlarmTime.setText(getString(R.string.no_active_alarm));
			btnCancel.setVisibility(View.GONE);
		} else if (SharedPrefsHandler.getAppState() == SharedPrefsHandler.STATE_ALARM) {
			tvLabel.setText(getString(R.string.alarm_will_ring_at) + " ");
			tvAlarmTime.setText(controller.getAlarmTime());
			btnCancel.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * Registers the listener for the button.
	 */
	private void registerListener() {
		btnCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				alarm.cancelAlarm(ContextPasser.getLocalContext());
				SharedPrefsHandler.setAlarmTime(0);
				SharedPrefsHandler.setAppState(SharedPrefsHandler.STATE_IDLE);
				Toast.makeText(ContextPasser.getLocalContext(), R.string.alarm_deactivated , Toast.LENGTH_SHORT).show();
				handleStates();
			}
		});
	}

	/**
	 * Sets the time displayed by the alarm time TextView.
	 * 
	 * @param time
	 *            The time to be displayed.
	 */
	public void setTime(String time) {
		tvAlarmTime.setText(time);
	}

	/**
	 * Sets the message displayed by the label TextView.
	 * 
	 * @param text
	 *            The message to be displayed.
	 */
	public void setLabel(String text) {
		tvLabel.setText(text);
	}

	/**
	 * Sets the visibility of the button.
	 * 
	 * @param visible
	 *            Indicates whether or not the button should be visible.
	 */
	public void setButtonVisible(boolean visible) {
		if (visible)
			btnCancel.setVisibility(View.VISIBLE);
		else
			btnCancel.setVisibility(View.GONE);
	}

	/**
	 * Sets the controller for the fragment.
	 * 
	 * @param controller
	 *            The controller for the fragment.
	 */
	public void setController(CtrlrTime controller) {
		this.controller = controller;
	}
}
