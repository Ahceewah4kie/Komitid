package se.entities;


/**
 * A class that represent a checkpoint
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class Checkpoint {
	private int segment;
	private double lat, lng;
	private long time;
	private String name;
	
	public double getLat() {
		return lat;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public long getTime() {
		return time;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	public int getSegment() {
		return segment;
	}
	
	public void setSegment(int segment) {
		this.segment = segment;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Checkpoint [segment=" + segment + ", lat=" + lat + ", lng=" + lng + ", time=" + time + ", name=" + name + "]";
	}
}
