package se.apihandlers;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import se.komitid.R;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import se.storage.Logger;
import se.storage.SharedPrefsHandler;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Sends a query to Trafiklab.se to get the checkpoints and saves them in db.
 * TODO: DO NOT FORGET TO REMOVE API KEY FROM VISIBLE DEBUG MESSAGES
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 */
public class TrafiklabHandler {
	private final String API_KEY = "CRWLFOMppOKFJvrerCOumP235lPCDLEl";
	private final String tag = "Komitid | TrafiklabHandler";

	private String errorMsg = "";
	private Context context = ContextPasser.getLocalContext();
	private String citynameFrom;
	private String citynameTo;
	private double depLat;
	private double depLng;
	private double arrLat;
	private double arrLng;
	private long arrTime;
	private boolean includeTrains = true;
	private boolean includeBuses = true;
	private boolean includeBoats = true;
	private double walkingSpeed;
	private boolean testMode = true;
	private DBAdapter db;
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public TrafiklabHandler() {
		db = new DBAdapter(ContextPasser.getLocalContext());
	}

	public void setDBAdapter(DBAdapter db) {
		this.db = db;
	}

	/**
	 * Prepares the query to be sent to Trafiklab. execute() must be explicitly
	 * called afterwards.
	 * 
	 * @param citynameFrom
	 *            - Name of departure-place
	 * @param cityNameto
	 *            - Name of arrival-place
	 * @param arrLat
	 *            - Latitude of arrival/goal
	 * @param arrLng
	 *            - Longitude of arrival/goal
	 * @param depLat
	 *            - Latitude of departure/start
	 * @param depLng
	 *            - Latitude of departure/start
	 * @param arrTime
	 *            - Time of desired arrival
	 * @param includeTrains
	 *            - Include trains in query?
	 * @param includeBuses
	 *            - Include buses in query?
	 * @param includeBoats
	 *            - Include boats in query?
	 * @param walkingSpeed
	 *            - Walking speed in KM/H
	 */
	public void prepareTripQuery(String citynameFrom, String cityNameto, double arrLat, double arrLng, double depLat, double depLng, long arrTime,
			boolean includeTrains, boolean includeBuses, boolean includeBoats, double walkingSpeed) {
		this.citynameFrom = citynameFrom;
		this.citynameTo = cityNameto;
		this.depLat = depLat; // Y-coordinate
		this.depLng = depLng; // X-coordinate
		this.arrLat = arrLat; // Y-coordinate
		this.arrLng = arrLng; // X-coordinate
		this.arrTime = arrTime;
		this.includeTrains = includeTrains;
		this.includeBuses = includeBuses;
		this.includeBoats = includeBoats;
		this.walkingSpeed = walkingSpeed;
	}

	/**
	 * Sends a query to Trafiklab.se/resrobot, to get checkpoints.
	 */
	public class SendTripQuery extends AsyncTask<String, Void, Boolean> {

		private ProgressDialog progressDiag;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDiag = new ProgressDialog(ContextPasser.getLocalContext());
			progressDiag.setMessage(context.getString(R.string.getting_trip_data));
			progressDiag.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i(tag, "Task running!");
			return sendQuery();
		}

		@Override
		protected void onPostExecute(Boolean success) {
			super.onPostExecute(success);

			progressDiag.dismiss();

			if (!success)
				showTripErrorDialog();
		}
	}

	/**
	 * Sends a query to Trafiklab, and handles the response. Note: MUST BE RUN
	 * IN AN ASYNCTASK!
	 * 
	 * @return 'true' if successful, otherwise 'false'
	 */
	public boolean sendQuery() {
		String query = getQuery();

		// Sends query to Trafiklab.
		try {
			URL url = new URL(query);
			String result = "";

			// Uses a 'fixed' query if debug mode is active, else a real query
			// to Trafiklabs
			if (SharedPrefsHandler.getDebugMode()) {
				result = IOUtils.toString(context.getResources().openRawResource(R.raw.test_trip));
			} else {
				result = IOUtils.toString(url, "ISO-8859-1");
			}
			Logger.i(tag, result);

			// Fixes the most weird bug I've seen ever since Ben was called
			// Obi-Wan.
			result = result.replace("{\"timetableresult\":{\"ttitem\":[", "");

			// Saves response to DB.
			try {
				JSONObject json = new JSONObject(result);
				Logger.i(tag, url.toString());
				dropAllRowsInDatabase();
				if (json.toString().equals("{\"timetableresult\":{}}")) {
					errorMsg = context.getString(R.string.error_notripfound);
				} else {

					if (SharedPrefsHandler.getDebugMode())
						interprentJSONDebug(json);
					else
						interpretJSON(json);
				}

				if (SharedPrefsHandler.getDebugMode()) {
					return true;
				} else {
					// Checks if trip is possible
					if (isTripPossible()) {
						Logger.i(tag, "Will save to database, see isTripPossible()");
						return true;
					} else {
						Logger.i(tag, "Will not save to database,see isTripPossible()");
						dropAllRowsInDatabase();
						return false;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Performs a series of checks to ensure that the trip is possible.
	 * 
	 * @return 'true' if all checks are passed, otherwise 'false'
	 */
	public boolean isTripPossible() {
		db.open();
		Cursor departures = db.getAllDepartures();
		Cursor arrivals = db.getAllArrivls();

		departures.moveToFirst();
		arrivals.moveToLast();

		long firstDepTime = departures.getLong(4);
		long lastArrTime = arrivals.getLong(4);
		long prepTime = SharedPrefsHandler.getPreparationTime();
		long desiredArrival = SharedPrefsHandler.getArrivalTime();
		long currentTime = System.currentTimeMillis();
		db.close();

		// Debug (Values)
		// Converts milliseconds to human readable dates and time
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
		DateTime dtlastArrTime = new DateTime(lastArrTime);
		DateTime dtdesiredArrival = new DateTime(desiredArrival);
		DateTime dtfirstDepTime = new DateTime(firstDepTime);
		DateTime dtcurrentTime = new DateTime(currentTime);
		DateTime dtprepTime = new DateTime(prepTime);
		String stlastArrTime = dtlastArrTime.toString(dtf);
		String stdesiredArrival = dtdesiredArrival.toString(dtf);
		String stfirstDepTime = dtfirstDepTime.toString(dtf);
		String stcurrentTime = dtcurrentTime.toString(dtf);
		String stprepTime = dtprepTime.toString(dtf);

		Logger.i(tag, "Last arrival: " + stlastArrTime);
		Logger.i(tag, "Desired arrival: " + stdesiredArrival);
		Logger.i(tag, "First departure: " + stfirstDepTime);
		Logger.i(tag, "Current: " + stcurrentTime);
		Logger.i(tag, "Prep time: " + stprepTime);
		//

		if ((firstDepTime - prepTime) < currentTime) {
			errorMsg = context.getString(R.string.error_departurebeforenow);
			Logger.i(tag, errorMsg);
			return false;
		} else if (lastArrTime > desiredArrival) {
			errorMsg = context.getString(R.string.error_arrivalistoolate);
			Logger.i(tag, "errorMsg");
			return false;
		} else {
			Logger.i(tag, "Resa möjlig!");
			return true;
		}
	}

	// Debug. See interpretJSON down below.
	private void interprentJSONDebug(JSONObject response) {
		try {
			int segmentLoops = 1;
			segmentLoops = response.getJSONArray("segment").length() - 1;
			int i = 0;
			while (i <= segmentLoops) {
				JSONObject mostRelevantDeparture = response.getJSONArray("segment").optJSONObject(i);
				JSONObject arrival = mostRelevantDeparture.getJSONObject("arrival");
				JSONObject departure = mostRelevantDeparture.getJSONObject("departure");
				JSONObject arrCoordinates = arrival.getJSONObject("location");
				JSONObject depCoordinates = departure.getJSONObject("location");

				// Departure
				int segmentJSON = i;
				String depNameJSON = depCoordinates.getString("name");
				double depLatJSON = depCoordinates.getDouble("@y");
				double depLngJSON = depCoordinates.getDouble("@x");
				String depTimeJSON = departure.getString("datetime");
				long depDateTimeJSON = System.currentTimeMillis() + (60000 * (i + 1));
				int distanceJSON = mostRelevantDeparture.optJSONObject("segmentid").optInt("distance");
				String transportMethodJSON = mostRelevantDeparture.getJSONObject("segmentid").getJSONObject("mot").getString("#text");

				String depCarrierName = "";
				String depCarrierNumber = "";
				String depCarrierId = "";
				String depCarrierUrl = "";

				try {
					depCarrierName = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("name");
					depCarrierNumber = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("number");
					depCarrierId = mostRelevantDeparture.optJSONObject("segmentid").getJSONObject("carrier").optString("id");
					depCarrierUrl = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("url");
				} catch (Exception e) {
					Log.i(tag, "No carrier available. This is an somewhat expected error - no worries :-)");
				}

				// Arrival
				String arrNameJSON = arrCoordinates.getString("name");
				double arrLatJSON = arrCoordinates.getDouble("@y");
				double arrLngJSON = arrCoordinates.getDouble("@x");
				String arrTimeJSON = arrival.getString("datetime");
				long arrDateTimeJSON = System.currentTimeMillis() + (90000 * (i + 1));
				int arrIDJSON = arrCoordinates.optInt("@id");

				// Debug
				Logger.i(tag, "=================| " + i + " |===============");
				Logger.i(tag, "Segment=" + segmentJSON);
				Logger.i(tag, "depName=" + depNameJSON);
				Logger.i(tag, "depLat=" + depLatJSON);
				Logger.i(tag, "depLng=" + depLngJSON);
				Logger.i(tag, "depDateTime=" + depDateTimeJSON);
				Logger.i(tag, "distance=" + distanceJSON);
				Logger.i(tag, "transportType=" + transportMethodJSON);
				Logger.i(tag, "depCarrierName=" + depCarrierName);
				Logger.i(tag, "depCarrierId=" + depCarrierId);
				Logger.i(tag, "depCarrierUrl=" + depCarrierUrl);
				Logger.i(tag, "arrNameJSON=" + arrNameJSON);
				Logger.i(tag, "arrLat=" + arrLatJSON);
				Logger.i(tag, "arrLng=" + arrLngJSON);
				Logger.i(tag, "arrDateTimeJSON=" + arrDateTimeJSON);
				Logger.i(tag, "=====================================\n");

				// Saves to db
				storeDeparture(segmentJSON, depLatJSON, depLngJSON, depNameJSON, depDateTimeJSON, transportMethodJSON, distanceJSON,
						depCarrierNumber, depCarrierId, depCarrierUrl, depCarrierName);
				storeArrival(segmentJSON, arrLatJSON, arrLngJSON, arrNameJSON, arrDateTimeJSON, arrIDJSON);
				i++;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Interprets the response from Trafiklab, and writes it to database
	 * 
	 * @param response
	 *            - the response from Trafiklab as a JSONObject
	 */
	private void interpretJSON(JSONObject response) {
		try {
			int segmentLoops = 1;
			segmentLoops = response.getJSONArray("segment").length() - 1;
			int i = 0;
			while (i <= segmentLoops) {
				JSONObject mostRelevantDeparture = response.getJSONArray("segment").optJSONObject(i);
				JSONObject arrival = mostRelevantDeparture.getJSONObject("arrival");
				JSONObject departure = mostRelevantDeparture.getJSONObject("departure");
				JSONObject arrCoordinates = arrival.getJSONObject("location");
				JSONObject depCoordinates = departure.getJSONObject("location");

				// Departure
				int segmentJSON = i;
				String depNameJSON = depCoordinates.getString("name");
				double depLatJSON = depCoordinates.getDouble("@y");
				double depLngJSON = depCoordinates.getDouble("@x");
				String depTimeJSON = departure.getString("datetime");
				long depDateTimeJSON = dateFormatter.parse(depTimeJSON).getTime();
				int distanceJSON = mostRelevantDeparture.optJSONObject("segmentid").optInt("distance");
				String transportMethodJSON = mostRelevantDeparture.getJSONObject("segmentid").getJSONObject("mot").getString("#text");

				String depCarrierName = "";
				String depCarrierNumber = "";
				String depCarrierId = "";
				String depCarrierUrl = "";

				try {
					depCarrierName = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("name");
					depCarrierNumber = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("number");
					depCarrierId = mostRelevantDeparture.optJSONObject("segmentid").getJSONObject("carrier").optString("id");
					depCarrierUrl = mostRelevantDeparture.optJSONObject("segmentid").optJSONObject("carrier").optString("url");
				} catch (Exception e) {
					Log.i(tag, "No carrier available. This is an somewhat expected error - no worries :-)");
				}

				// Arrival
				String arrNameJSON = arrCoordinates.getString("name");
				double arrLatJSON = arrCoordinates.getDouble("@y");
				double arrLngJSON = arrCoordinates.getDouble("@x");
				String arrTimeJSON = arrival.getString("datetime");
				long arrDateTimeJSON = dateFormatter.parse(arrTimeJSON).getTime();
				int arrIDJSON = arrCoordinates.optInt("@id");

				// Debug
				Logger.i(tag, "=================| " + i + " |===============");
				Logger.i(tag, "Segment=" + segmentJSON);
				Logger.i(tag, "depName=" + depNameJSON);
				Logger.i(tag, "depLat=" + depLatJSON);
				Logger.i(tag, "depLng=" + depLngJSON);
				Logger.i(tag, "depDateTime=" + depDateTimeJSON);
				Logger.i(tag, "distance=" + distanceJSON);
				Logger.i(tag, "transportType=" + transportMethodJSON);
				Logger.i(tag, "depCarrierName=" + depCarrierName);
				Logger.i(tag, "depCarrierId=" + depCarrierId);
				Logger.i(tag, "depCarrierUrl=" + depCarrierUrl);
				Logger.i(tag, "arrNameJSON=" + arrNameJSON);
				Logger.i(tag, "arrLat=" + arrLatJSON);
				Logger.i(tag, "arrLng=" + arrLngJSON);
				Logger.i(tag, "arrDateTimeJSON=" + arrDateTimeJSON);
				Logger.i(tag, "=====================================\n");

				storeDeparture(segmentJSON, depLatJSON, depLngJSON, depNameJSON, depDateTimeJSON, transportMethodJSON, distanceJSON,
						depCarrierNumber, depCarrierId, depCarrierUrl, depCarrierName);
				storeArrival(segmentJSON, arrLatJSON, arrLngJSON, arrNameJSON, arrDateTimeJSON, arrIDJSON);
				i++;
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Drops all rows from all tables in database.
	 */
	private void dropAllRowsInDatabase() {
		db.open();
		db.dropArrivals();
		db.dropDepartures();
		db.close();
	}

	/**
	 * Saves a departure to db
	 * 
	 * @param segment
	 *            - which segment
	 * @param lat
	 *            - latitude of departure
	 * @param lng
	 *            - longitude of departure
	 * @param name
	 *            - name of the place of departure
	 * @param datetime
	 *            - time of departure in milliseconds
	 * @param transportMethod
	 *            - method of transport, eg. train
	 * @param distance
	 *            - distance to arrival (not in use, since we calculate that on
	 *            ourself)
	 * @param depCarrierNumber
	 *            - number of carrier (primarily used to identify buses and in
	 *            some cases trains)
	 * @param depCarrierId
	 *            - id of carrier, currently not in use
	 * @param depCarrierUrl
	 *            - url to the carrier company, not currently in use
	 * @param depCarrierName
	 *            - name of carrier company, ie. Skånetrafiken
	 */
	private void storeDeparture(int segment, double lat, double lng, String name, long datetime, String transportMethod, int distance,
			String depCarrierNumber, String depCarrierId, String depCarrierUrl, String depCarrierName) {
		db.open();
		db.addDeparture(segment, lat, lng, name, datetime, transportMethod, distance, depCarrierNumber, depCarrierId, depCarrierUrl, depCarrierName);
		db.close();
	}

	/**
	 * Saves an arrival to db
	 * 
	 * @param segment
	 *            - which segment
	 * @param lat
	 *            - latitude of arrival
	 * @param lng
	 *            - longitude of arrival
	 * @param name
	 *            - name of the place of arrival
	 * @param datetime
	 *            - time of arrival in milliseconds
	 * @param id
	 *            - id of carrier, currently not in use
	 */
	private void storeArrival(int segment, double lat, double lng, String name, long datetime, int id) {
		db.open();
		db.addArrival(segment, lat, lng, name, datetime, id);
		db.close();
	}

	/**
	 * Pops an alertdialog to inform the user of any errors when sending queries
	 * to Trafiklab.
	 * 
	 * @return an AlertDialog
	 */
	public AlertDialog showTripErrorDialog() {
		AlertDialog.Builder diagError = new AlertDialog.Builder(ContextPasser.getLocalContext());
		diagError.setTitle(context.getText(R.string.error_tripnotpossible));
		diagError.setMessage(errorMsg);
		diagError.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		return diagError.show();
	}

	/**
	 * A complete query, with the users preferences. TODO: REMOVE
	 * QUERY-PRINTING. API-KEY IS SECRET!
	 * 
	 * @return A complete query, with the users preferences.
	 */
	private String getQuery() {
		// TODO: Replace "resrobotsuper" with "resrobot" for faster response
		// , but not as fresh information about delays etc.
		String query = "https://api.trafiklab.se/samtrafiken/resrobotsuper/Search";

		DateTimeFormatter dtfTime = DateTimeFormat.forPattern("HH:mm");
		DateTimeFormatter dtfDate = DateTimeFormat.forPattern("yyyy-MM-dd");
		DateTime tempArrTime = new DateTime(arrTime);
		String time = tempArrTime.toString(dtfTime);
		String date = tempArrTime.toString(dtfDate);

		Log.i(tag, "time=" + time);
		Log.i(tag, "date=" + date);

		// Set answer to JSON
		query += ".json";

		// Set API key
		query += "?key=" + API_KEY;

		// Set name of city of departure
		query += "&from=" + citynameFrom;// context.getResources().getString(R.string.placeA);

		// Set name of city of arrival
		query += "&to=" + citynameTo; // context.getResources().getString(R.string.placeB);

		// Set the coordinate system
		query += "&coordSys=" + "WGS84";

		// Add departure longitude
		query += "&fromY=" + depLat;

		// Add departure latitude
		query += "&fromX=" + depLng;

		// Add arrival longitude
		query += "&toY=" + arrLat;

		// Add arrival latitude
		query += "&toX=" + arrLng;

		// Is trains allowed?
		query += "&mode2=" + includeTrains;

		// Is buses allowed?
		query += "&mode3=" + includeBuses;

		// Is boats allowed?
		query += "&mode4=" + includeBoats;

		// Set time to arrival time if true, instead of departure time
		query += "&arrival=" + true;

		// Set walking speed M/S
		query += "&walkSpeed=" + (walkingSpeed / 3600) * 1000;

		// Set time HH:MM (24h)
		query += "&time=" + time;

		// Set date YYYY-MM-DD
		query += "&date=" + date;

		// Set API version to 2.1
		query += "&apiVersion=2.1";

		Logger.i(tag, "query=" + query);

		return query;
	}

}
